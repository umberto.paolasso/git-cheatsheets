## Git: Lista dei Comandi Utili

---


Qui di seguito una lista di comandi utili di Git e una breve descrizione per ciascuno.


### Creazione e Clonazione del Repository

Per iniziare ad utilizzare Git, è necessario creare o clonare un repository.

- `git init`: crea un nuovo repository Git vuoto nella directory corrente.

- `git clone <url>`: crea una copia locale di un repository esistente, specificando l'URL del repository.


### Staging e Commit

Per salvare le modifiche nella repository, è necessario prima aggiungere i file alla staging area e poi eseguire il commit.

- `git add <file>`: aggiunge il file specificato alla staging area.

- `git add .`: aggiunge tutti i file modificati alla staging area.

- `git commit -m "messaggio del commit"`: salva le modifiche nella repository locale con il messaggio specificato.

  

### Push e Pull

Dopo aver eseguito il commit, è possibile caricare le modifiche nella repository remota o scaricare le modifiche dalla repository remota.

- `git push`: carica le modifiche nella repository remota.

- `git pull`: scarica le modifiche dalla repository remota.

  

### Branch e Merge

Per sviluppare diverse funzionalità contemporaneamente, è possibile utilizzare i branch e successivamente unire i cambiamenti tramite il merge.


- `git branch`: visualizza l'elenco dei branch e crea o elimina un branch.

- `git checkout <branch>`: sposta il branch corrente su un altro branch o su una commit specifica.

- `git merge <branch>`: unisce un branch in un altro branch.

  

### Altri Comandi Utili

Esistono molti altri comandi utili di Git che potrebbero essere necessari a seconda del tuo caso d'uso specifico.

- `git status`: visualizza lo stato attuale dei file nella repository.

- `git diff <commit> <commit>`: visualizza le differenze tra due commit.

- `git log`: visualizza la cronologia dei commit nella repository.

- `git reset`: annulla le modifiche e ripristina la repository allo stato precedente.

- `git stash`: salva temporaneamente le modifiche senza eseguire il commit.

- `git tag`: contrassegna una commit specifica con un tag.



## Appunti più approfonditi


## INFO GENERICHE

---

  
Git è un sistema di controllo versione distribuito, che permette di tenere traccia delle modifiche apportate a un insieme di file nel tempo. In pratica, Git consente di gestire la storia delle modifiche di un progetto, in modo da poter lavorare in modo collaborativo, tenere traccia delle modifiche apportate da diversi utenti e recuperare facilmente versioni precedenti dei file.

  
Git è particolarmente utile per i progetti software, dove molte persone possono lavorare contemporaneamente sullo stesso codice sorgente. Con Git, ogni membro del team può lavorare in un branch separato, per poi unire i propri cambiamenti in una branch principale comune, mantenendo così la storia delle modifiche e la tracciabilità dei cambiamenti.

  
Inoltre, Git consente di lavorare in modalità offline, ovvero senza una connessione internet attiva, grazie alla possibilità di creare repository locali sul proprio computer. Questo è particolarmente utile per lavorare su progetti quando non si dispone di una connessione internet stabile o in presenza di restrizioni di sicurezza che impediscono l'accesso ai repository online.

  
In sintesi, Git serve per gestire in modo efficace la storia delle modifiche di un progetto, semplificando il lavoro di sviluppo in team e agevolando la gestione dei conflitti e delle versioni dei file.

  
  

## Comandi Base

---

Ecco una breve descrizione dei comandi base di Git:

git init: questo comando inizializza un nuovo repository Git nella directory corrente. Questo significa che Git inizierà a tenere traccia delle modifiche ai file in quella directory.

  
`git add nomeFile`  -> questo comando aggiunge uno o più file alla staging area, ovvero la zona in cui vengono preparati i cambiamenti da salvare nel repository. In pratica, i file aggiunti con git add sono pronti per essere committati.

  
`git add `. -> per aggiungere tutti i file si usa

  
`git commit`  ->questo comando salva le modifiche nella staging area nel repository. Ogni commit viene accompagnato da un messaggio di commit che descrive brevemente le modifiche apportate.

  
`git status`  ->questo comando mostra lo stato del repository, ovvero quali file sono stati modificati, quali sono pronti per essere committati e quali sono stati esclusi dalla gestione di versione.

  

`git log` -> questo comando mostra la lista dei commit effettuati nel repository, insieme alle relative informazioni come autore, data e messaggio di commit.

  

`git branch` -> questo comando mostra l'elenco dei branch presenti nel repository. Un branch rappresenta una linea di sviluppo separata, e può essere utilizzato per lavorare su nuove funzionalità o correzioni di bug senza interferire con il codice principale.

  

`git checkout nomeBranch` ->questo comando permette di spostarsi tra i diversi branch presenti nel repository. È possibile utilizzare git checkout anche per ripristinare una versione precedente dei file.

  
  

## Creazione di Branch

---

  

Per **creare un nuovo branch in Git**, puoi utilizzare il comando git branch seguito dal nome del nuovo branch che vuoi creare. Ad esempio, per creare un nuovo branch chiamato "my-feature-branch", puoi digitare il seguente comando nella tua console Git:

`git branch nomeBranch`

  
Tieni presente che questo comando crea solo il nuovo branch, ma non ti sposta automaticamente su di esso. **Per spostarti sul nuovo branch**, devi utilizzare il comando git checkout seguito dal nome del branch. Ad esempio:

`git checkout my-feature-branch

Questo comando ti sposterà sul nuovo branch appena creato, dove puoi iniziare a lavorare e apportare le modifiche necessarie. In alternativa, puoi utilizzare il comando git checkout -b per **creare un nuovo branch e spostarti automaticamente** su di esso in una sola operazione. Ad esempio:

`git checkout -b my-feature-branch`

  

Questo comando crea il nuovo branch "my-feature-branch" e ti sposta automaticamente su di esso. Ora puoi iniziare a lavorare sul nuovo branch e apportare le modifiche necessarie senza influire sulla branch principale del tuo repository.

  
    

Per **visualizzare la diramazione dei vari branch** in un repository Git, puoi utilizzare il comando` git log  --graph`. Questa opzione mostra una rappresentazione grafica del repository, evidenziando la posizione dei vari branch e delle loro intersezioni.

  

Per visualizzare il grafico dei commit insieme alle diramazioni dei branch, puoi digitare il seguente comando nella tua console Git:

`git log --graph`


Questo comando mostrerà un grafico a caratteri ASCII che rappresenta la storia dei commit nel tuo repository, evidenziando le varie diramazioni dei branch e le intersezioni tra di esse. Ogni commit è rappresentato da un nodo nel grafico, mentre le linee che collegano i nodi rappresentano la storia delle modifiche ai file del repository.

  

Inoltre, puoi utilizzare l'opzione --decorate per visualizzare anche i nomi dei branch e dei tag nei commit corrispondenti nel grafico. Ad esempio:

`git log --graph --decorate`

  
  

Questo comando mostrerà un grafico simile al precedente, ma con l'aggiunta delle informazioni sui nomi dei branch e dei tag corrispondenti ai vari commit. In questo modo, puoi avere una visione più completa della struttura del tuo repository e delle varie diramazioni dei branch.


## Spostarsi tra branch

---


quando ha senso usare **git switch vs git checkout** per cambiare branch su git?


`git switch` è un comando introdotto in Git 2.23 e progettato per essere un modo più intuitivo per cambiare branch rispetto al vecchio comando git checkout. Tuttavia, entrambi i comandi possono essere utilizzati in modo intercambiabile per cambiare branch in Git.


In generale, git switch è preferibile a git checkoutquando si cambia branch perché offre una sintassi più semplice e più chiara, inoltre è più sicuro perché impedisce il "detached HEAD state", che può causare problemi se si commette accidentalmente su un branch sbagliato o si perde il riferimento al commit corrente.


git checkout è ancora utile in alcuni casi, ad esempio quando si desidera creare un nuovo branch dal commit corrente, creare una nuova branch partendo da una branch esistente o quando si vuole spostarsi tra branch e file contemporaneamente.

In generale, è possibile utilizzare entrambi i comandi per cambiare branch su Git, ma git switch è la scelta preferibile in caso di necessità di una sintassi più intuitiva e sicura.

  
  

## Git merge

---


git merge è un comando di Git che consente di unire le modifiche di un branch in un altro. In pratica, il comando git merge prende le modifiche apportate in un branch e le unisce con quelle presenti in un altro branch, creando un nuovo commit che rappresenta la fusione dei due branch.

  
Per eseguire un **merge in Git**, è necessario posizionarsi sul branch di destinazione (cioè quello in cui si vogliono unire le modifiche) e utilizzare il comando git merge seguito dal nome del branch da cui si vogliono importare le modifiche. Ad esempio, per unire le modifiche dal branch "feature" nel branch "main", si può digitare il seguente comando nella console Git:

```
git checkout main

git merge feature
```

Questo comando crea un nuovo commit che rappresenta la fusione dei due branch, unendo le modifiche apportate in "feature" con quelle presenti in "main". In questo modo, le modifiche apportate nel branch "feature" diventano parte del branch "main".

Per eliminare un merge in Git, è possibile utilizzare il comando git reset. Tuttavia, eliminare un merge può portare alla perdita di dati e causare problemi di coerenza nel repository, quindi è necessario fare attenzione e valutare attentamente le conseguenze.

  
  
Per **eliminare un merge**, è necessario eseguire il comando git reset con l'opzione --hard seguita dall'ID del commit precedente al merge. Ad esempio, per eliminare l'ultimo merge eseguito nel branch "main", si può digitare il seguente comando nella console Git:

```
git checkout main

git reset --hard HEAD
```

Questo comando annulla il commit del merge più recente e riporta il repository allo stato precedente alla fusione dei due branch. Tuttavia, questa operazione può causare la perdita di dati e la creazione di conflitti nel repository, quindi è necessario utilizzarla con cautela e solo se si è sicuri di voler eliminare il merge.


ho anche usato questo comando per cancellare un branch->(https://www.w3schools.com/git/git_branch_merge.asp?remote=github)

  

``git branch -d emergency-fix``



  

HEAD--> Ci dice dove sta puntando il nostro git, dove sta guardando

  

git log --graph--pretty=oneline

  
  

git diff primoArogmento Secondo(usato per vedere )

-> si puo scrivere subito git diff Secondo

  
  


git nomecosa --help

  

si puo prendere id di un log passasto


per creare da questo un nuovo branch usare:

git switch - c nomeNuovoBranch

  
  

convenzione sui nomi branch per

branch nuovi con features feat/nome

branch nuovi con features fix/nome

  

risoluzuone dei conflitti su una stessa riga, andano a lavorare direttamnet li togliendo la head >>>> e il file differente che vogliamo mergera che fa la differenza

  
  

## Gitlab

### Push e Pull

---

  
  

creazione di account su gitlab possibilità di collegarlo tramite chiave ssh o http

  

crezione di un nuovo folder su gitlab

  

collegare i due folder(quello locale e quello online),

aggiungere il collegamento sul file locale,

  

una volta collegati è possibile fare un push dalla cartella locale alla remota

per scaricare una cartella remota invece si deve fare un pull

riscrivi in formato md e dammi il codice

  

### Dettagli

GitLab è una piattaforma di gestione di repository Git, che consente ai team di sviluppo di collaborare alla creazione e gestione del codice sorgente. Tra le funzionalità principali di GitLab ci sono le operazioni di push e pull, che permettono di aggiornare il codice all'interno di un repository.

  

Per fare un push su GitLab, è necessario avere un repository locale già inizializzato e configurato per GitLab. Una volta che si è pronti per condividere le modifiche apportate al codice sorgente, si può eseguire il comando "git push" per inviare le modifiche al repository remoto di GitLab.

  

Il push è utile quando si vuole condividere il proprio lavoro con gli altri membri del team, permettendo loro di accedere alle modifiche e continuare a lavorare sul progetto.

  

Per fare un pull su GitLab, invece, si può eseguire il comando "git pull" per scaricare le modifiche apportate da altri membri del team nel repository remoto e sincronizzarle con il repository locale.

  

Il pull è utile quando si lavora in team su un progetto e si vogliono aggiornare le proprie modifiche in base a quelle degli altri membri del team.

  

In sintesi, push e pull sono due operazioni fondamentali di GitLab che permettono di condividere e sincronizzare il codice sorgente tra i membri del team, garantendo una gestione efficace e collaborativa del progetto.

Per fare il push:

  

`git push origin <branch-name>`

  

dove `<branch-name>` è il nome del branch locale in cui si hanno effettuato le modifiche.

  

Per fare il pull:

  

`git pull origin <branch-name>`

  

dove `<branch-name>` è il nome del branch remoto da cui si vogliono scaricare le modifiche.

  

In entrambi i casi, è necessario essere posizionati all'interno della directory del repository locale e aver già configurato il repository remoto di GitLab con il comando `git remote add origin <repository-url>`.